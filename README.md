Before you clone this repository remember that you need to have the latest version of virtualbox (5.0.16+) and vagrant (1.8.1+) at the time of writing. Every major release of this repository is ensured to run on all major platforms (OSX 10.11+, Windows 8+, Ubuntu 14.04+) unless stated otherwise in the release.

After cloning this repository the first command should do a

**`vagrant up`**


This will clone the **[superleap/centos-71-x64](https://atlas.hashicorp.com/superleap/boxes/centos-71-x64)** base vagrant box which has platform flavor of virtual machine guest utilities and git installed.

The first time you run vagrant up the script will install all the required software on the box via the equivalent command:

**`vagrant provision`**

The provision script doesn't require manual runs it should only be used to change settings as needed.

What you want to do after is add all vhosts you are going to use to your system wide hosts file. This creates a DNS record on your system linking the specified domain name to the IP of your vagrant box.

On OSX and Linux, this is located at **`/etc/hosts`**, on Windows it is located at **`C:\Windows\System32\drivers\etc\hosts`**.

**`dev.`** versions of each vhost are added to the alias list. You can switch between the production and development version of a website by only aliasing the `dev.domain` version.

Add a line with the IP address of this server and all vhosts you chose. For example, if the IP address is **`192.168.99.95`**, and you chose vhost **`continer.xyz`**, you will want to enter the following:

**`192.168.99.95   dev.continer.xyz`**

Some websites had an attached CDN domain. For example if the IP address is **`192.168.99.95`**, and you chose vhost **`continer.xyz`**, you will want to enter the following:

**`192.168.99.95   dev.continer.xyz cdn.continer.xyz`**

Further documentation regarding CDN domains will be available soon.

If you forgot the IP address you chose, check the config file at **`./deployment/puppet/config.yaml`**.

